#!/usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-
# By HarJIT in 2020.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

""" Minimalist cpio exclusively supporting the ar (.a) format """

import sys, os, getopt, functools, io, re, fnmatch, binaryfile

def fail(error, exit=1):
    print(error, file=sys.stderr)
    sys.exit(exit)

def int_or_fail(file, length, base, error):
    try:
        data = file.read(length)
        assert len(data) == length, data
        data = data.decode("utf-8", errors="surrogateescape").rstrip(" ")
        return int(data, base) if data else None # meta fields for // file can be completely empty
    except ValueError:
        fail(error)

def check_pats(fn, pats, inv_pat):
    if not pats:
        return True
    for pat in pats:
        if fnmatch.fnmatch(fn, pat):
            return not inv_pat
    else: # for…else
        return inv_pat

def suck_file(raw_in, body_length, out_file=None):
    read_already = 0
    while read_already < body_length:
        read_length = min(io.DEFAULT_BUFFER_SIZE, body_length - read_already)
        data = raw_in.read(read_length)
        actual_read = len(data)
        if out_file is not None: # note: stdin is not seekable for fairly obvious reasons
            out_file.write(data)
        read_already += actual_read

long_fn_idx_ref = re.compile(r"(?a)/(\d+)\Z")
# Binutils and LLVM will use slash-linefeed to end a name in the extended names index, while at
#   least some MSVC `.lib`s seem to use NUL.
long_fn_idx_delim = re.compile(r"/\n|\x00")
long_fn_prepend_ref = re.compile(r"(?a)#1/(\d+)\Z")

try:
    opts, args = getopt.gnu_getopt(sys.argv[1:], "iotv?d0mfurI:O:F:E:D:", ["convert-backslashes", "insecure", "automatic-rename", "extract-ranlibs"])
except getopt.GetoptError:
    opts = args = () # i.e. fall thru to help screen.

mode = "help"
verbose = False
directories = False
backslashes = False
mtimes = False
delimiter = "\n"
inv_pat = False
ar_fn = None
overwrite_mode = "newer_or_unstamped"
pats = []
insecure = False
patterns_filename = None
target_directory = None
extract_ranlibs = False
for opt, param in opts:
    # -ti is the same as -t, since -t is technically a submode of -i in cpio (not in pax)
    if opt == "-i" and mode != "list":
        mode = "unpack"
    elif opt == "-o":
        mode = "pack"
    elif opt == "-t":
        mode = "list"
    elif opt == "-?":
        mode = "help"
    elif opt == "-v":
        verbose = True
    elif opt == "-d":
        directories = True
    elif opt == "-m":
        mtimes = True
    elif opt == "-0":
        delimiter = "\0"
    elif opt == "-f":
        inv_pat = True
    elif opt == "-u":
        overwrite_mode = "unconditional"
    elif opt == "-r":
        overwrite_mode = "rename"
    elif opt == "--automatic-rename":
        overwrite_mode = "autorename"
    elif opt in ("-I", "-O", "-F"):
        ar_fn = param
    elif opt == "-E":
        patterns_filename = param
    elif opt == "-D":
        target_directory = param
    elif opt == "--insecure":
        insecure = True
    elif opt == "--convert-backslashes":
        backslashes = True
    elif opt == "--extract-ranlibs":
        extract_ranlibs = True
if args:
    if mode == "unpack":
        pats = args
    else:
        mode = "help"

file_list_newline_mode = ("\n" if os.name == "posix" else None)

if patterns_filename:
    with open(patterns_filename, "r", encoding="utf-8", errors="surrogateescape",
            newline=file_list_newline_mode) as f:
        pats.extend(f.read().strip(delimiter).split(delimiter))

if mode == "help":
    print("-i: extract. -t: list (tabulate). -o: pack. (defaults to stdio)", file=sys.stderr)
    print("-D: directory to extract to. -O/-I/-F: archive file name.", file=sys.stderr)
    print("-0 (zero): paths on stdin sep by NUL not LF.", file=sys.stderr)
    print("-f: invert extraction constraint filename patterns.", file=sys.stderr)
    print("-E: read more extraction constraint patterns (beside arguments) from filename.", file=sys.stderr)
    print("-v: verbose. -d: create directories. -m: store/extract modify times.", file=sys.stderr)
    print("-r: interactive rename. -u: overwrite even when timestamp stored & older.", file=sys.stderr)
    print("--automatic-rename: automatically rename colliding file names on extract.", file=sys.stderr)
    print("--insecure: extract to parent dirs, absolute paths, symlinks...", file=sys.stderr)
    print("--convert-backslashes: convert backslashes in paths in archive to slashes.", file=sys.stderr)
    print("--extract-ranlibs: extract library indices to a file.", file=sys.stderr)
elif mode == "pack":
    utf8_stdin = binaryfile.clone_open(0, os.O_RDONLY, "r", encoding="utf-8", errors="surrogateescape",
                    newline=file_list_newline_mode)
    raw_out = binaryfile.clone_open(1, os.O_WRONLY, "wb", buffering=0) if not ar_fn \
                else open(ar_fn, "wb", buffering=0)
    raw_out.write(b"!<arch>\n")
    things = utf8_stdin.read().strip(delimiter).split(delimiter)
    for i in things:
        if verbose:
            print(i, file=sys.stderr)
        filemode = oct(os.stat(i).st_mode).lstrip("o0").encode("ascii")
        filemode += (b" " * (8 - len(filemode)))
        fndat = i.encode("utf-8", errors="surrogateescape")
        prefix = b""
        if (len(fndat) > 16) or (" " in i) or ("/" in i):
            prefix, fndat = fndat, "#1/{:d}".format(len(fndat)).encode("ascii")
            assert len(fndat) < 16
        fndat += (b" " * (16 - len(fndat)))
        stat_obj = os.stat(i)
        length = stat_obj.st_size + len(prefix)
        if mtimes:
            timestamp = "{:d}".format(int(stat_obj.st_mtime)).encode("utf-8")
            timestamp += (b" " * (12 - len(timestamp)))
            assert len(timestamp) <= 12
        else:
            timestamp = b"0" + (b" " * 11)
        uid = b"0" + (b" " * 5)
        gid = b"0" + (b" " * 5)
        length_out = f"{length:d}".encode("ascii")
        length_out += (b" " * (10 - len(length_out)))
        raw_out.write(fndat + timestamp + uid + gid + filemode + length_out + b"`\n" + prefix)
        with open(i, "rb") as file_in:
            while data_in := file_in.read(io.DEFAULT_BUFFER_SIZE):
                raw_out.write(data_in)
        if length % 2:
            raw_out.write(b"\n")
else:
    long_name_table = None
    raw_in = binaryfile.clone_open(0, os.O_RDONLY, "rb") if not ar_fn else open(ar_fn, "rb")
    if raw_in.read(8) != b"!<arch>\n":
        print("unrecognised magic: not an ar (.a) archive", file=sys.stderr)
    if target_directory is not None:
        abs_target_directory = os.path.abspath(target_directory)
        target_directory_parent = os.path.dirname(abs_target_directory)
        if directories and target_directory_parent:
            os.makedirs(target_directory_parent, exist_ok=True)
        os.chdir(target_directory)
    ranlib_number = 0
    while 1:
        fn = raw_in.read(16)
        if not fn:
            break
        assert len(fn) == 16, fn
        fn = fn.decode("utf-8", errors="surrogateescape").rstrip(" ")
        timestamp = int_or_fail(raw_in, 12, 10, "invalid timestamp")
        uid = int_or_fail(raw_in, 6, 10, "invalid uid")
        gid = int_or_fail(raw_in, 6, 10, "invalid gid")
        filemode = int_or_fail(raw_in, 8, 8, "invalid file mode")
        length = int_or_fail(raw_in, 10, 10, "invalid length")
        header_term = raw_in.read(2)
        if header_term != b"`\n":
            fail("presumed header terminator is not backtick-linefeed")
        #
        if fn == "//":
            long_name_table = raw_in.read(length).decode("utf-8", errors="surrogateescape")
        else:
            do_extract = (mode == "unpack")
            #
            if fn in ("/", "/SYM64/"):
                if not extract_ranlibs:
                    do_extract = False
                else:
                    fn = "__RANLIB__/{:04d}{}".format(ranlib_number, fn.strip("/"))
                    ranlib_number += 1
            body_length = length
            if long_name_table and (match_obj := long_fn_idx_ref.match(fn)):
                fn = long_fn_idx_delim.split(long_name_table[int(match_obj.group(1), 10):], 1)[0]
            elif match_obj := long_fn_prepend_ref.match(fn):
                fn_length = int(match_obj.group(1), 10)
                fn = raw_in.read(fn_length).decode("utf-8", errors="surrogateescape")
                # Don't edit `length` directly since it's used for expecting padding bytes.
                body_length -= len(fn)
            elif fn.endswith("/"):
                fn = fn[:-1]
            #
            if backslashes and "\\" in fn:
                fn = fn.replace("\\", "/")
            do_extract = do_extract and check_pats(fn, pats, inv_pat)
            if (do_extract and verbose) or (mode == "list"):
                if (mode == "list") and verbose:
                    fail("long format listing not implemented")
                else:
                    print(fn, file=sys.stderr)
            #
            if do_extract:
                if not insecure:
                    if os.path.isabs(fn):
                        fail(f"path {fn!r} is absolute")
                    dfn = fn
                    while (pair := os.path.split(dfn)) != ("", ""):
                        dfn, bfn = pair
                        # Note: Windows 9x allows, in my experience, ..., ...., etc. as
                        #   parpardir, parparpardir, et cetera. Though WinNT/Linux/etc don't.
                        if (bfn == os.path.pardir) or (bfn != "." and not bfn.strip(".")):
                            fail(f"path {fn!r} has parent directory components")
                        elif os.path.islink(os.path.join(dfn, bfn)):
                            fail(f"path {fn!r} contains symbolic links")
                #
                if os.path.exists(fn):
                    if overwrite_mode == "newer_or_unstamped":
                        do_extract = (timestamp > os.stat(fn).st_mtime) or not timestamp
                    elif overwrite_mode == "unconditional":
                        pass
                    elif overwrite_mode == "rename":
                        print(end = f"Rename {fn!r} to?: ", file = sys.stderr)
                        sys.stderr.flush()
                        if ar_fn is not None: # stdin not in use for archive input
                            fn = input()
                        else:
                            if os.name == "posix":
                                with open("/dev/tty", "r") as terminal:
                                    fn = terminal.readline().rstrip("\n")
                            else:
                                import msvcrt
                                fnarray = []
                                while (char := msvcrt.getwche()) not in "\n\r":
                                    if char == "\b":
                                        fnarray and fnarray.pop()
                                    elif char == "\x03": # Contra documentation, we can swallow ^C
                                        print("\b ", file = sys.stderr)
                                        raise KeyboardInterrupt() # User hit ^C
                                    else:
                                        fnarray.append(char)
                                fn = "".join(fnarray)
                                print(file = sys.stderr) # Otherwise tends to CR without LFing(!)
                    elif overwrite_mode == "autorename":
                        for i in range(10000):
                            new_fn = f"{fn}-{i:04d}"
                            if not os.path.exists(new_fn):
                                fn = new_fn
                        else:
                            fail(f"cannot automatically suitably rename {fn!r}")
                #
                if directories and (dfn := os.path.dirname(fn)):
                    os.makedirs(dfn, exist_ok=True)
                with open(fn, "wb") as out_file:
                    suck_file(raw_in, body_length, out_file=out_file)
                if mtimes:
                    os.utime(fn, (os.stat(fn).st_atime, timestamp))
            else:
                suck_file(raw_in, body_length, out_file=None)
        #
        if length % 2:
            should_be_linefeed = raw_in.read(1)
            if should_be_linefeed != b"\n":
                fail("presumed pad byte is not linefeed")



