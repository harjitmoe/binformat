#!/usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-
# By HarJIT in 2020.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import struct, io, os

class BinaryFile(object):
    """Class wrapping a FileIO or BytesIO object to provide easy access to binary values in files
    or byte strings, even those not aligned to bytes.
    """
    def __init__(self, wrappee, big_endian=False):
        self.wrappee = wrappee
        self.big_endian = big_endian
        self.bitwise_offset = 0
        self.pending = 0
        self.detached = False
    @property
    def seekable(self):
        if hasattr(self.wrappee, "getvalue"):
            return True
        return self.wrappee.seekable()
    @property
    def writable(self):
        if hasattr(self.wrappee, "getvalue"):
            return True
        return self.wrappee.writable()
    @property
    def readable(self):
        if hasattr(self.wrappee, "getvalue"):
            return True
        return self.wrappee.readable()
    def _force_byte(self):
        # Get a byte or, if EOF, shove a NUL, but make a seek advancement happen irrespective,
        #   unless the wrapped file is read-only.
        # Low level tool used by other methods, hence does not align to bitwise_offset
        nice_way = self.wrappee.read(1)
        if nice_way:
            return nice_way[0], False
        elif self.writable:
            self.wrappee.write(b"\0")
            return 0, True
        else:
            return 0, True
    def _soft_byte(self):
        # Get a byte or, if EOF, a NUL, but don't lastingly advance the seek either way.
        save = self.wrappee.tell()
        nice_way = self.wrappee.read(1)
        self.wrappee.seek(save)
        if nice_way:
            return nice_way[0]
        else:
            return 0
    @property
    def endian_char(self):
        return ">" if self.big_endian else "<"
    def seek(self, byte_offset, bitwise_offset):
        if not self.readable or not self.seekable:
            raise ValueError("seeking an non-random-access file")
        if not (0 <= bitwise_offset < 8):
            raise ValueError("bitwise offset not in 0 thru 7 inclusive")
        self.wrappee.seek(byte_offset)
        self.bitwise_offset = bitwise_offset
    def tell(self):
        return self.wrappee.tell(), self.bitwise_offset
    def read_bytes(self, length):
        assert 0 <= self.bitwise_offset < 8
        if self.bitwise_offset == 0:
            return self.wrappee.read(length)
        if self.seekable:
            # Won't be aligned since that's handled by shortcut case at start
            slotty = self.wrappee.read(length + 1)
            padding = b"\0" * ((length + 1) - len(slotty))
            if self.writable:
                # Force the seek advancement to be exactly equal irrespective
                self.wrappee.write(padding)
            slotty += padding
            self.wrappee.seek(-1, 1)
        else:
            slotty = bytes([self.pending]) + self.wrappee.read(length)
            self.pending = slotty[-1]
            slotty += b"\0" * ((length + 1) - len(slotty))
        out = bytearray([0] * length)
        for i in range(len(out)):
            if self.big_endian:
                out[i] = ((slotty[i] << self.bitwise_offset) |
                          (slotty[i + 1] >> (8 - self.bitwise_offset))) & 0xFF
            else:
                out[i] = ((slotty[i] >> self.bitwise_offset) |
                          (slotty[i + 1] << (8 - self.bitwise_offset))) & 0xFF
        return bytes(out)
    def write_bytes(self, bytestring):
        assert 0 <= self.bitwise_offset < 8
        if self.bitwise_offset == 0:
            self.wrappee.write(bytestring)
            return
        slotty = b"\0" + bytestring + b"\0"
        out = bytearray([0] * (len(bytestring) + 1))
        for i in range(len(out)):
            if self.big_endian:
                out[i] = ((slotty[i] << (8 - self.bitwise_offset)) |
                          (slotty[i + 1] >> self.bitwise_offset)) & 0xFF
            else:
                out[i] = ((slotty[i] >> (8 - self.bitwise_offset)) |
                          (slotty[i + 1] << self.bitwise_offset)) & 0xFF
        if self.readable:
            startpos = self.wrappee.tell()
            initbyte, eof = self._force_byte()
            self.wrappee.seek(startpos + len(bytestring)) # = startpos + len(out) - 1
            finbyte, eof = self._force_byte()
            self.wrappee.seek(startpos)
        else:
            initbyte = self.pending
            finbyte = 0
        if self.big_endian:
            initbyte &= (~((1 << (8 - self.bitwise_offset)) - 1)) & 0xFF
            finbyte &= (1 << (8 - self.bitwise_offset)) - 1
        else:
            initbyte &= (1 << self.bitwise_offset) - 1
            finbyte &= (~((1 << self.bitwise_offset) - 1)) & 0xFF
        out[0] |= initbyte
        out[-1] |= finbyte
        # Always won't be aligned by this point, since that's handled by shortcut case at start
        if self.readable:
            self.wrappee.write(out)
            self.wrappee.seek(-1, 1)
        else:
            self.wrappee.write(out[:-1])
            self.pending = out[-1]
    def read_bits(self, count=1):
        ret = 0
        nextset = 1
        advance_next = False
        eof = False
        if self.seekable:
            byte, eof = self._force_byte()
        elif self.bitwise_offset:
            byte = self.pending
        else:
            advance_next = True
        for i in range(count):
            if advance_next:
                assert not self.seekable # Only used in that case
                # On a seekable stream, we want to consistently advance seek when bitwise_offset
                #   becomes zero so we can unconditionally seek back after the loop ends, i.e.
                #   the current byte upon the loop ending should be the first to be read from next.
                # On an unseekable stream, we want to not read more from the stream unless
                #   absolutely necessary, and to know that pending attr is not relevant whenever
                #   bitwise_offset is zero. Hence doing this only when (re)reaching the loop start.
                temp = self.wrappee.read(1)
                if temp:
                    byte = self.pending = temp[0]
                else:
                    byte = self.pending = 0
                    eof = True
                advance_next = False
            if eof:
                return None
            if self.big_endian:
                val = byte & (0x80 >> self.bitwise_offset)
            else:
                val = byte & (1 << self.bitwise_offset)
            self.bitwise_offset += 1
            if self.bitwise_offset == 8:
                self.bitwise_offset = 0
                if self.seekable:
                    byte = self._soft_byte()
                else:
                    advance_next = True
            #
            if self.big_endian:
                ret <<= 1
            ret |= bool(val) and nextset
            if not self.big_endian:
                nextset <<= 1
        if self.seekable:
            self.wrappee.seek(-1, 1)
        return ret
    def write_bits(self, value, count):
        nextset = 1 if not self.big_endian else 1 << (count - 1)
        if self.readable:
            byte = self._soft_byte()
        else:
            byte = self.pending
        for i in range(count):
            boolean = bool(value & nextset)
            if self.big_endian:
                nextset >>= 1
                poker = 0x80 >> self.bitwise_offset
            else:
                nextset <<= 1
                poker = 1 << self.bitwise_offset
            byte |= poker # Is now set
            byte ^= poker # Is now unset
            byte |= (boolean and poker) # Is now set to the value of boolean
            self.bitwise_offset += 1
            if self.bitwise_offset == 8:
                self.bitwise_offset = 0
                self.wrappee.write(bytes([byte]))
                if self.readable:
                    byte = self._soft_byte()
                else:
                    byte = self.pending = 0
        if self.bitwise_offset:
            if self.readable:
                self.wrappee.write(bytes([byte]))
                self.wrappee.seek(-1, 1)
            else:
                self.pending = byte
    def read_short16(self, signed=False):
        if self.big_endian:
            high, low = tuple(self.read_bytes(2))
        else:
            low, high = tuple(self.read_bytes(2))
        retval = (high << 8) | low
        if signed:
            retval |= -(1 << 16)
        return retval
    def read_long32(self, signed=False):
        if self.big_endian:
            top, up, down, bottom = tuple(self.read_bytes(4))
        else:
            bottom, down, up, top = tuple(self.read_bytes(4))
        retval = (top << 24) | (up << 16) | (down << 8) | bottom
        if signed:
            retval |= -(1 << 32)
        return retval
    def read_long64(self, signed=False):
        if self.big_endian:
            top, truth, charm, up, down, strange, beauty, bottom = tuple(self.read_bytes(8))
        else:
            bottom, beauty, strange, down, up, charm, truth, top = tuple(self.read_bytes(8))
        retval = ((top << 56) | (truth << 48) | (charm << 40) | (up << 32) |
                  (down << 24) | (strange << 16) | (beauty << 8) | bottom)
        if signed:
            retval |= -(1 << 64)
        return retval
    def read_float16(self):
        return struct.unpack(self.endian_char + "e", self.read_bytes(2))[0]
    def read_float32(self):
        return struct.unpack(self.endian_char + "f", self.read_bytes(4))[0]
    def read_float64(self):
        return struct.unpack(self.endian_char + "d", self.read_bytes(8))[0]
    def _write_val(self, value, bytewidth):
        array = []
        temp = value if not hasattr(value, "value") else value.value # Accept ctypes objects
        for i in range(bytewidth):
            array.append(temp & 0xFF)
            temp >>= 8
        if temp not in (0, -1):
            raise ValueError("overflow")
        if self.big_endian:
            array.reverse()
        self.write_bytes(bytes(array))
    def write_short16(self, value):
        self._write_val(value, 2)
    def write_long32(self, value):
        self._write_val(value, 4)
    def write_long64(self, value):
        self._write_val(value, 8)
    def write_float16(self, value):
        temp = value if not hasattr(value, "value") else value.value # Accept ctypes objects
        self.write_bytes(struct.pack(self.endian_char + "e", temp))
    def write_float32(self, value):
        temp = value if not hasattr(value, "value") else value.value # Accept ctypes objects
        self.write_bytes(struct.pack(self.endian_char + "f", temp))
    def write_float64(self, value):
        temp = value if not hasattr(value, "value") else value.value # Accept ctypes objects
        self.write_bytes(struct.pack(self.endian_char + "d", temp))
    def read_string(self, length=None, encoding="utf-8", errors="strict", bytewidth=None):
        if not bytewidth:
            norm = encoding.casefold().replace("-", "").replace("_", "")
            if norm.startswith(("utf16", "cseucfixwid")):
                bytewidth = 2
            elif norm.startswith("utf32"):
                bytewidth = 4
            else:
                bytewidth = 1
        out = []
        count = 0
        while length is None or count < length:
            bit = self.read_bytes(bytewidth)
            if length is None and not bit.rstrip("\0"):
                break
            out.extend(list(bit))
            count += 1
        return bytes(out).decode(encoding, errors=errors)
    def getvalue(self):
        return self.wrappee.getvalue()
    def detach(self):
        if not self.detached:
            self.detached = True
            if self.bitwise_offset and self.writable and not self.readable:
                self.wrappee.write(bytes([self.pending]))
            self.wrappee = None
    def close(self):
        save_wrappee = self.wrappee
        self.detach()
        if save_wrappee is not None:
            return save_wrappee.close()
    def __del__(self):
        self.detach()

def openbin(fn, big_endian=False):
    return BinaryFile(open(fn, "rb+", buffering=0), big_endian=big_endian)

def stringbin(data, big_endian=False):
    return BinaryFile(io.BytesIO(data), big_endian=big_endian)

def clone_open(fd, flags, mode, **kwargs):
    if os.name == "posix":
        pid = os.getpid()
        new_fd = os.open(f"/proc/{pid:d}/fd/{fd:d}", flags)
    elif os.name == "nt":
        import msvcrt
        handle = msvcrt.get_osfhandle(fd)
        new_fd = msvcrt.open_osfhandle(handle, flags)
    else:
        raise RuntimeError(f"cannot clone file descriptor on {os.name!r}")
    return os.fdopen(new_fd, mode, **kwargs)



