#!/usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-
# By HarJIT in 2020.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import re, getopt, sys, os
import binaryfile

# This is a 6-bit, as opposed to 7-bit, Fieldata variant (the 7-bit variants used extra characters
#   with the high bit *unset*; contrast Extended ASCII which extends with the high bit *set).
#   It is a modal/stateful variant, with UC/LC controls for switching case. I'm also using the case
#   to switch between some inter-variant alternatives, e.g. percent versus double quote.
lowercase = (
    0x40, 0x0F, 0x5D, 0x09, 0x0A, 0x20, 0x61, 0x62,
    0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A,
    0x6B, 0x6C, 0x6D, 0x6E, 0x6F, 0x70, 0x71, 0x72,
    0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7A,
    0x29, 0x2D, 0x2B, 0x3C, 0x3D, 0x3E, 0x5F, 0x24,
    0x2A, 0x28, 0x22, 0x3A, 0x3F, 0x21, 0x2C, 0x1F6D1,
    0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
    0x38, 0x39, 0x27, 0x3B, 0x2F, 0x2E, 0xA4, 0x08)
uppercase = (
    0x40, 0x5B, 0x0E, 0x23, 0x0D, 0x20, 0x41, 0x42,
    0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A,
    0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50, 0x51, 0x52,
    0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A,
    0x29, 0x2D, 0x2B, 0x3C, 0x3D, 0x3E, 0x26, 0x24,
    0x2A, 0x28, 0x25, 0x3A, 0x3F, 0x21, 0x2C, 0x5C,
    0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
    0x38, 0x39, 0x27, 0x3B, 0x2F, 0x2E, 0xA4, 0x2260)

repertoire = frozenset(uppercase) | frozenset(lowercase)
repertoire ^= {0x0E, 0x0F} # Don't actually generate case shifts for ASCII shifts.

def escape(inp, escape_ampersand=False):
    """ Escape a string of characters for encoding in the Fieldata variant implemented
    in this module. This is similar to xmlcharrefreplace (WHATWG's "html" error mode).

    Setting escape_ampersand=True escapes the ampersand itself as well, as appropriate
    for non-SGML contexts.
    """
    temp = inp
    if escape_ampersand:
        temp = temp.replace("&", "&#38;")
    return "".join(i if ord(i) in repertoire else "&#{:d};".format(ord(i)) for i in temp)

blot = re.compile(r"(&#[0123456789]+;)") # Outer brackets so delimeter gets included in split array
def unescape(inp, unescape_ampersand=False):
    """ Unescape SGML decimal character references, except those which alter the syntax.

    Exception: setting unescape_ampersand=True unescapes the ampersand itself as well,
    in order to reverse the effects of escape(…) with escape_ampersand=True set.
    """
    out = []
    for i in blot.split(inp):
        if blot.match(i):
            c = chr(int(i[2:-1], 10))
            if (c in "<>\"'") or (c == "&" and not unescape_ampersand):
                # Avoid expansions which will syntactically alter HTML or XML. But if existing
                #   ampersands got escaped (i.e. via escape_ampersand=True), unescape them.
                out.append(i)
            else:
                out.append(c)
        else:
            out.append(i)
    return "".join(out)

def pack(inp, out, sgml=False, big_endian=False):
    """ Pack from a Unicode IO stream into a Fieldata binary IO stream.
    
    Set sgml=True to not ampersand-escape the ampersand itself. Set big_endian=True for big endian.
    """
    stuff = binaryfile.BinaryFile(out, big_endian=big_endian)
    capital = True
    for line in inp:
        for c in escape(line, escape_ampersand=(not sgml)):
            i = ord(c)
            mapping = uppercase if capital else lowercase
            if i not in mapping:
                if capital:
                    capital = False
                    mapping = lowercase
                    stuff.write_bits(0x02, 6)
                else:
                    capital = True
                    mapping = uppercase
                    stuff.write_bits(0x01, 6)
            stuff.write_bits(mapping.index(i), 6)
    if stuff.tell()[1] == 2:
        # In these cases, there seems to be a spurious @ sign at the end of the stream due to there
        #   being exactly six remaining bits before the byte boundary. Avoid this with an extra
        #   case shift at the end.
        stuff.write_bits(0x02 if capital else 0x01, 6)
        assert stuff.tell()[1] == 0
    stuff.detach()

def unpack(inp, out, sgml=False, big_endian=False):
    """ Unpack into a Unicode IO stream from a Fieldata binary IO stream.
    
    Set sgml=True to not unescape the ampersand itself when expanding the decimal SGML escapes.
    Set big_endian=True for big endian.
    """
    stuff = binaryfile.BinaryFile(inp, big_endian=big_endian)
    capital = True
    amper = []
    while 1:
        mapping = uppercase if capital else lowercase
        i = stuff.read_bits(6)
        if i is None:
            return
        c = chr(mapping[i])
        if c == "\x0E":
            capital = False
        elif c == "\x0F":
            capital = True
        elif amper:
            if len(amper) >= 3 and c == ";":
                out.write(unescape("".join(amper) + c, unescape_ampersand=(not sgml)))
                del amper[:]
            elif ((len(amper) >= 3 and c not in "0123456789;") or # caution: "٣".isdigit() is True
                  (len(amper) == 1 and c != "#") or
                  (len(amper) == 2 and c not in "0123456789")):   # likewise.
                out.write("".join(amper) + c)
                del amper[:]
            else:
                amper.append(c)
        elif c == "&":
            amper.append(c)
        else:
            out.write(c)
    if amper:
        out.write("".join(amper))

def naive_unpack(inp, out, big_endian=False):
    stuff = binaryfile.BinaryFile(inp, big_endian=big_endian)
    while 1:
        i = stuff.read_bits(6)
        if i is None:
            return
        c = chr(uppercase[i])
        if c == "\x0E":
            c = "]"
        out.write(c)

if __name__ == "__main__":
    opts, args = getopt.gnu_getopt(sys.argv[1:], "rwnBs")
    mode = "encode"
    big_endian = False
    sgml = False
    for opt, param in opts:
        if opt == "-r":
            mode = "decode"
        elif opt == "-n":
            mode = "naive_decode"
        elif opt == "-w":
            mode = "encode"
        elif opt == "-B":
            big_endian = True
        elif opt == "-s":
            sgml = True
    raw_stdin = binaryfile.clone_open(0, os.O_RDONLY, "rb")
    utf8_stdin = binaryfile.clone_open(0, os.O_RDONLY, "r", encoding="utf-8", errors="surrogateescape", newline="\n")
    raw_stdout = binaryfile.clone_open(1, os.O_WRONLY, "wb")
    utf8_stdout = binaryfile.clone_open(1, os.O_WRONLY, "w", encoding="utf-8", errors="surrogateescape", newline="\n")
    if mode == "encode":
        outdata = pack(utf8_stdin, raw_stdout, sgml=sgml, big_endian=big_endian)
        raw_stdout.flush()
    elif mode == "naive_decode":
        naive_unpack(raw_stdin, utf8_stdout, big_endian=big_endian)
        utf8_stdout.flush()
    else:
        unpack(raw_stdin, utf8_stdout, sgml=sgml, big_endian=big_endian)
        utf8_stdout.flush()





