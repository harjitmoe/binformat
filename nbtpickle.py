#!/usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-
# By HarJIT in 2019, 2020.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

""" Use the pickle/json/marshal basic API with NBT data. """

from ctypes import c_byte, c_int16, c_int32, c_int64, c_float, c_double
import io, collections
import binaryfile

END, BYTE, SHORT, INT, LONG, FLOAT, DOUBLE, BYTES, \
STRING, LIST, COMPOUND, INTS, LONGS = tuple(range(13))

class OrderedDict(collections.OrderedDict):
    def __repr__(self):
        return "{" + ", ".join("{!r}: {!r}".format(*i) for i in self.items()) + "}"

# Note that the nbttypewhenempty is only referenced as a last resort, i.e. for an empty array.
# The idea being so empty arrays round-trip if they're part of a part of the structure which
# the script loading/editing/dumping it doesn't do anything to.
# The original type itself might be a placeholder (END or BYTE) in the case of an empty list. So
# the content takes priority.
# Note also that the nbttypewhenempty does not mean the same thing between them: for NBTList it's
# the INNER type and for NBTTuple it's the OUTER type. For fairly obvious reasons.
class NBTList(list):
    """Subclass of list which uses a specific element type when empty and saved to NBT."""
    def __init__(self, typ, *args):
        self.nbttypewhenempty = typ
        super().__init__(*args)
class NBTTuple(tuple):
    """Subclass of tuple which uses a specific array type when empty and saved to NBT."""
    # The tuple class itself overrides __new__ so trying to add the typ argument
    # by overriding __init__ does something close to squat.
    def __new__(cls, typ, *args):
        self = super().__new__(cls, *args)
        self.nbttypewhenempty = typ
        return self

# The idea's to be useful for serialising native Python stuff as well as the Minecraft stuff.
def _deduce_type(obj):
    if isinstance(obj, c_byte):
        return BYTE
    elif isinstance(obj, c_int16):
        return SHORT
    elif isinstance(obj, c_int32):
        return INT
    elif isinstance(obj, c_int64):
        return LONG
    elif isinstance(obj, int):
        # If passed as a Python type, assume type is insignificant and use shortest possible
        # representation for that integer.
        ref = obj if obj >= 0 else ~obj
        if not (ref >> 7):
            return BYTE
        elif not (ref >> 15):
            return SHORT
        elif not (ref >> 31):
            return INT
        elif not (ref >> 63):
            return LONG
        else:
            raise ValueError("integer too large to be stored in NBT ({!r})".format(obj))
    elif isinstance(obj, c_float):
        return FLOAT
    elif isinstance(obj, (c_double, float)):
        # Python's native "float" is actually double-precision.
        # Since the different float types are as much a matter of precision as maximum,
        # don't mess around trying to multiplex them like with integers.
        return DOUBLE
    elif isinstance(obj, (bytes, bytearray)):
        return BYTES
    elif isinstance(obj, str):
        return STRING
    elif isinstance(obj, list):
        return LIST
    elif isinstance(obj, dict):
        return COMPOUND
    elif isinstance(obj, tuple):
        if not obj:
            if isinstance(obj, NBTTuple):
                return obj.nbttypewhenempty
            raise TypeError("cannot automatically represent untyped empty tuple")
        #
        # For a tuple solely of native Python integers:
        can32bit = True
        for i in obj:
            if not isinstance(i, int):
                break
            ref = i if i >= 0 else ~i
            can32bit = can32bit and not (ref >> 31)
        else: # i.e. finished without encountering break
            # Don't use BYTES, since that won't round-trip to a tuple.
            return INTS if can32bit else LONGS
        #
        # For anything else:
        inner = _deduce_type(obj[0])
        for i in obj:
            if isinstance(i, int):
                raise ValueError("mixed native and fixed-width integers in a tuple")
            if _deduce_type(i) != inner:
                raise ValueError("mixed-type tuples forbidden in NBT")
        if inner == BYTE: # We've excluded native integers, so this is necessarily a c_byte.
            return BYTES
        elif inner == INT:
            return INTS
        elif inner == LONG:
            return LONGS
        else:
            return LIST
    else:
        raise TypeError("unsupported type {!r}".format(type(obj)))

def _deduce_list_type(lst):
    if not lst:
        if isinstance(lst, NBTList):
            return lst.nbttypewhenempty
        else: # Empty list without a set type.
            return BYTE # alternatively END, but that apparently breaks some older parsers.
    status = None
    for item in lst:
        if isinstance(item, int) and status in (None, -1):
            status = -1
        elif status is None:
            status = _deduce_type(item)
        elif status == _deduce_type(item):
            pass
        elif (status == -1) and isinstance(item, (c_byte, c_int16, c_int32, c_int64)):
            raise ValueError("mixed native and fixed-width integers in a list")
        elif (status != -1) and isinstance(item, int):
            raise ValueError("mixed native and fixed-width integers in a list")
        else:
            raise ValueError("mixed-type lists forbidden in NBT")
    if status == -1:
        typ = BYTE
        for i in lst:
            ref = i if i >= 0 else ~i
            if typ in (BYTE,) and (ref >> 7):
                typ = SHORT
            if typ in (BYTE, SHORT) and (ref >> 15): # not elif
                typ = INT
            if typ in (BYTE, SHORT, INT) and (ref >> 31): # not elif
                typ = LONG
            if typ in (BYTE, SHORT, INT, LONG) and (ref >> 63): # not elif
                raise ValueError("integer too large to be stored in NBT ({!r})".format(i))
        return typ
    elif status == -2:
        return DOUBLE
    else:
        return status

def _dump(item, file, typ):
    # GENERAL NOTE: not using bytes() on the ctypes types besides c_byte means I don't have to care
    # about the host's native formats.
    if typ == BYTE:
        if isinstance(item, c_byte):
            file.write_bytes(bytes(item))
        else:
            # bytes() requires an unsigned representation, hence the &0xFF.
            file.write_bytes(bytes([item & 0xFF]))
    elif typ == SHORT:
        file.write_short16(item)
    elif typ == INT:
        file.write_long32(item)
    elif typ == LONG:
        file.write_long64(item)
    elif typ == FLOAT:
        file.write_float32(item)
    elif typ == DOUBLE:
        file.write_float64(item)
    elif typ == BYTES:
        file.write_long32(len(item))
        # Might be passed to us as bytes, bytearray or tuple
        if item and isinstance(item[0], c_byte):
            # Accepted for consistency with such representations for INTS and LONGS but
            # not recommended; loads() will return a bytes object rather than this format.
            file.write(b"".join(bytes(i) for i in item))
        else:
            file.write(bytes(item))
    elif typ == STRING:
        bitem = item.encode("utf-8", errors="surrogateescape")
        file.write_short16(len(bitem))
        file.write_bytes(bitem)
    elif typ == LIST:
        inner = _deduce_list_type(item)
        file.write_bytes(bytes([inner]))
        file.write_long32(len(item))
        for i in item:
            _dump(i, file, inner)
    elif typ == COMPOUND:
        for (name, i) in item.items():
            typ = _deduce_type(i)
            if not isinstance(name, str):
                raise ValueError("keys must be strings")
            bname = name.encode("utf-8", errors="surrogateescape")
            file.write_bytes(bytes([typ]))
            file.write_short16(len(bname))
            file.write_bytes(bname)
            _dump(i, file, typ)
        file.write_bytes(bytes([END]))
    elif typ == INTS:
        file.write_long32(len(item))
        for element in item:
            file.write_long32(element)
    elif typ == LONGS:
        file.write_long32(len(item))
        for element in item:
            file.write_long64(element)
    else:
        raise RuntimeError("unexpected `typ` value; this should not happen ({!r})".format(typ))

def dump_bfile(obj, file, *, name=None):
    file.write_bytes(bytes([_deduce_type(obj)]))
    if name is None:
        file.write_short16(0)
    elif not isinstance(name, str):
        raise ValueError("keys must be strings")
    else:
        bname = name.encode("utf-8")
        file.write_short16(len(bname))
        file.write_bytes(bname)
    _dump(obj, file, _deduce_type(obj))

def dump(obj, file, *, name=None, big_endian=True):
    """ Write an object (obj) as uncompressed NBT to a file (file).
    
    The optional name argument gives a name to the root tag.
    
    If big_endian=True (the default), write the big-endian format. If
    big_endian=False, write the little-endian format (MCJE always uses the
    big-endian format, while MCBE appears to use both in different contexts).
    """
    return dump_bfile(obj, binaryfile.BinaryFile(file, big_endian=big_endian), name=name)

def dumps(obj, *, name=None, big_endian=True):
    """ Convert an object (obj) to uncompressed NBT byte data.
    
    The optional name argument gives a name to the root tag.
    
    If big_endian=True (the default), write the big-endian format. If
    big_endian=False, write the little-endian format (MCJE always uses the
    big-endian format, while MCBE appears to use both in different contexts).
    """
    f = io.BytesIO()
    dump(obj, f, name=name, big_endian=big_endian)
    return f.getvalue()

def _load(file, typ, preserve):
    if typ == BYTE:
        ret = file.read_bytes(1)[0]
        wrapper = c_byte
    elif typ == SHORT:
        ret = file.read_short16()
        wrapper = c_int16
    elif typ == INT:
        ret = file.read_long32()
        wrapper = c_int32
    elif typ == LONG:
        ret = file.read_long64()
        wrapper = c_int64
    elif typ == FLOAT:
        ret = file.read_float32()
        wrapper = c_float
    elif typ == DOUBLE:
        ret = file.read_float64()
        wrapper = c_double
    elif typ == BYTES:
        length = file.read_long32()
        ret = file.read(length)
        wrapper = lambda ret: ret
    elif typ == STRING:
        length = file.read_short16()
        ret = file.read_string(length=length, encoding="utf-8", errors="surrogateescape")
        wrapper = lambda ret: ret
    elif typ == LIST:
        inner = file.read_bytes(1)[0]
        length = file.read_long32()
        ret = [_load(file, inner, preserve) for i in range(length)]
        wrapper = lambda ret: NBTList(inner, ret)
    elif typ == COMPOUND:
        ret = OrderedDict({}) if preserve else {}
        while 1:
            name, i = load_bfile(file, preserve=preserve, with_name=True) # Not _load
            if i is None: # i.e. encountered an END tag
                break
            elif name not in ret:
                ret[name] = i
            else:
                raise ValueError("duplicate key: {!r}".format(name))
        wrapper = lambda ret: ret
    elif typ == INTS:
        length = file.read_long32()
        ret = tuple(file.read_long32() for i in range(length))
        wrapper = lambda ret: NBTTuple(typ, (c_int32(i) for i in ret))
    elif typ == LONGS:
        length = file.read_long32()
        ret = tuple(file.read_long64() for i in range(length))
        wrapper = lambda ret: NBTTuple(typ, (c_int64(i) for i in ret))
    else:
        raise ValueError("Unsupported NBT field type: {!r}. "
                         "This probably means wrong endian.".format(typ))
    return wrapper(ret) if preserve else ret

def load_bfile(file, *, preserve=True, with_name=False):
    typ = file.read_bytes(1)[0]
    if typ == END:
        return (None, None) if with_name else None
    namelen = file.read_short16()
    name = file.read_string(length=namelen, encoding="utf-8", errors="replace")
    tell = file.tell()
    #print(f"{name!r}, {typ!r}, {preserve!r}, {tell!r}")
    if with_name:
        return name, _load(file, typ, preserve)
    elif name:
        raise ValueError("named root tag without with_name=True")
    else:
        return _load(file, typ, preserve)

def load(file, *, preserve=True, with_name=False, big_endian=True):
    """ Read uncompressed NBT data from a file (file), return its content.
    
    If preserve=True (the default), efforts will be made to preserve the
    original NBT data as faithfully as possible (in particular, numerical
    values will be represented as ctypes wrappers). If preserve=False,
    values will be returned in their natural Python representations, and
    may not retain the same types if dumped again.

    If with_name=False (the default), the object will be returned; an
    error will be thrown if the root tag is named. If with_name=True,
    a tuple of the name and the object will be returned. Both of these
    are false in event of an END tag.
    
    If big_endian=True (the default), read the big-endian format. If
    big_endian=False, read the little-endian format (MCJE always uses the
    big-endian format, while MCBE appears to use both in different contexts).
    """
    return load_bfile(binaryfile.BinaryFile(file, big_endian=big_endian),
                      preserve=preserve, with_name=with_name)

def loads(dat, *, preserve=True, with_name=False, big_endian=True):
    """ Load an object from uncompressed NBT data (dat).
    
    If preserve=True (the default), efforts will be made to preserve the
    original NBT data as faithfully as possible (in particular, numerical
    values will be represented as ctypes wrappers). If preserve=False,
    values will be returned in their natural Python representations, and
    may not retain the same types if dumped again.

    If with_name=False (the default), the object will be returned; an
    error will be thrown if the root tag is named. If with_name=True,
    a tuple of the name (even if an empty string) and the object will 
    be returned. Both of these are None in event of an END tag.
    
    If big_endian=True (the default), read the big-endian format. If
    big_endian=False, read the little-endian format (MCJE always uses the
    big-endian format, while MCBE appears to use both in different contexts).
    """
    return load(io.BytesIO(dat), preserve=preserve, with_name=with_name, big_endian=big_endian)







